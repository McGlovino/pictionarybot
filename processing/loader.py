#import tensorflow.compat.v1 as tf
#tf.disable_v2_behavior() 
import tensorflow as tf
import pickle
import numpy as np

#no_inputs = 784  # input layer (28x28 pixels)
#no_hidden2 = 512  # 2nd hidden layer
no_outputs = 345  # output layer (0-9 digits)

file = open("500000\\x_test.obj",'rb')
object_file = pickle.load(file)
x_test = object_file
file.close()


file = open("500000\\y_test.obj",'rb')
object_file = pickle.load(file)
y_test = object_file
y_test = np.eye(no_outputs)[y_test]
file.close()
       

#Graph is not all loaded items from default graph
graph = tf.get_default_graph()

with tf.Session() as sess:
    #load items into default graph
    saver = tf.train.import_meta_graph('trained_network_pictobot.meta')
    saver.restore(sess, tf.train.latest_checkpoint('./'))
    #print(sess.run('accuracy:0'))
    #Test
    print("Testing on Unseen Data")
    _accuracy = graph.get_tensor_by_name('accuracy:0')
    print('Accuracy:', _accuracy.eval(
            {graph.get_tensor_by_name('X_1:0'): x_test, graph.get_tensor_by_name('Y_1:0'): y_test}))