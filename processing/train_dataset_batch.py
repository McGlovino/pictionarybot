import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()
#import tensorflow as tf 
import pickle
import numpy as np

import math
import time
import random


def choose_batches():
    batches_added = 1 #for time estimation
    x_return = []
    y_return = []
    x_return.extend(x_batches[batches_run])
    y_return.extend(y_batches[batches_run])
    drop_chance = 1+(batches_run*0.1)
    for i in range(batches_run):
        if random.uniform(0,drop_chance) < 1:
            x_return.extend(x_batches[i])
            y_return.extend(y_batches[i])
            batches_added = batches_added + 1
    return x_return, y_return, batches_added


no_inputs = 784  # input layer (28x28 pixels)
no_hidden1 = 700  # 1st hidden layer
no_hidden2 = 512  # 2nd hidden layer
#no_hidden3 = 256  # 3rd hidden layer
no_outputs = 345  # output layer (0-9 digits)

learning_rate = 0.0015 #1e-4 #one less zero than number
no_iterations = 10
batch_size = 5000
batches_run = 0
pass_no = 1
dropout = 0.5

print("Importing Data")
file = open("500000\\x_train.obj",'rb')
object_file = pickle.load(file)
x_train = object_file
file.close()

file = open("500000\\x_test.obj",'rb')
object_file = pickle.load(file)
x_test = object_file
file.close()

file = open("500000\\y_train.obj",'rb')
object_file = pickle.load(file)
y_train = object_file
y_train =  np.eye(no_outputs)[y_train]
file.close()

file = open("500000\\y_test.obj",'rb')
object_file = pickle.load(file)
y_test = object_file
y_test = np.eye(no_outputs)[y_test]
file.close()

#Split the batches
print("Creating Batches")
x_batches = []
y_batches = []
for i in range(math.ceil(len(x_train)/batch_size)):
    if ((i+1)*batch_size)-1 > len(x_train):
        x_batches.append(x_train[ (i*batch_size) : len(x_train)])
        y_batches.append(y_train[ (i*batch_size) : len(x_train)])
    else:
        x_batches.append(x_train[ (i*batch_size) : ((i+1)*batch_size)-1 ])
        y_batches.append(y_train[ (i*batch_size) : ((i+1)*batch_size)-1 ])
                                    

X = tf.placeholder("float", [None, no_inputs], name='X')
Y = tf.placeholder("float", [None, no_outputs], name='Y')
keep_prob = tf.placeholder(tf.float32)

weights = {
    'w1': tf.Variable(tf.truncated_normal([no_inputs, no_hidden1], stddev=0.1)),
    'w2': tf.Variable(tf.truncated_normal([no_hidden1, no_hidden2], stddev=0.1)),
    #'w3': tf.Variable(tf.truncated_normal([no_hidden2, no_hidden3], stddev=0.1)),
    'out': tf.Variable(tf.truncated_normal([no_hidden2, no_outputs], stddev=0.1)),
}

biases = {
    'b1': tf.Variable(tf.constant(0.1, shape=[no_hidden1])),
    'b2': tf.Variable(tf.constant(0.1, shape=[no_hidden2])),
    #'b3': tf.Variable(tf.constant(0.1, shape=[no_hidden3])),
    'out': tf.Variable(tf.constant(0.1, shape=[no_outputs]))
}

layer_1 = tf.add(tf.matmul(X, weights['w1']), biases['b1'])
layer_2 = tf.add(tf.matmul(layer_1, weights['w2']), biases['b2'])
#layer_3 = tf.add(tf.matmul(layer_2, weights['w3']), biases['b3'])
layer_drop = tf.nn.dropout(layer_2, keep_prob)
output_layer = tf.matmul(layer_2, weights['out'], name='output_layer') + biases['out']

cross_entropy = tf.reduce_mean(
    tf.nn.softmax_cross_entropy_with_logits(
        labels=Y, logits=output_layer # labels=Y
        ))
train_step = tf.train.AdamOptimizer(learning_rate).minimize(cross_entropy)

saver = tf.train.Saver()
with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    
    correct_pred = tf.equal(tf.argmax(output_layer, 1), tf.argmax(Y, 1), name='correct_pred')
    accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32), name='accuracy')
    
    end_of_batch = (batches_run+1)*batch_size
    batch_itterations_start = 0
    j = 0
    start = time.time()
    print("Integrated Batches = " + str(batches_run + 1))
    print("Pass: " + str(pass_no))
    x_run, y_run, no_batches_added = choose_batches()
    # train, integrating one batch at a time
    """while end_of_batch < len(x_train) + batch_size:
        if end_of_batch > len(x_train):
            end_of_batch = len(x_train)
        sess.run(train_step, feed_dict={
            X: x_run, Y: y_run, keep_prob: dropout
            })
        # print loss and accuracy
        if j % 10 == 0:
            itteration_loss, itteration_accuracy = sess.run(
                [cross_entropy, accuracy],
                feed_dict={X: x_run, Y: y_run, keep_prob: 1.0}
                )
            print(
                "Iteration",
                str(j),
                "\t| Loss =",
                str(itteration_loss),
                "\t| Accuracy =",
                str(itteration_accuracy)
                )
            if (itteration_accuracy > 0.3
            or ((j - batch_itterations_start) > 500 and itteration_accuracy > 0.2) 
            or ((j - batch_itterations_start) > 750 and itteration_accuracy > 0.1)
            or ((j - batch_itterations_start) > 1250 and itteration_accuracy > 0.05)
            or ((j - batch_itterations_start) > 2000 and itteration_accuracy > 0.025)
            or (j - batch_itterations_start) > 2750):
                batches_run = batches_run + 1
                batch_itterations_start = j
                end_of_batch = (batches_run+1)*batch_size
                
                time_remaining = 0
                end = time.time()
                for i in range((math.ceil(len(x_train)/batch_size)) - (batches_run)):
                    batch_drop_chance = 1+((batches_run + i)*0.15)
                    time_remaining = time_remaining + (((end-start) / no_batches_added) * ((batches_run + i)/(batch_drop_chance)) * (i * 0.5))
                if end-start < 60:
                    print("Batch Integration Took: {:.2f} seconds".format(end-start))
                elif (end-start)/60 < 60:
                    print("Batch Integration Took: {:.2f} minuites".format((end-start)/60))
                else:
                    print("Batch Integration Took: {:.2f} hours".format(((end-start)/60)/60))
                print("Estimated Time Remaining : {:.2f} hours".format((time_remaining / 60)/60))
                start = time.time()
                if end_of_batch <= len(x_train):
                    x_run, y_run, no_batches_added = choose_batches()
                    print("Integrating Batch: " + str(batches_run + 1) + " of " + str(math.ceil(len(x_train)/batch_size)))
                    print("Batches Added: " + str(no_batches_added))
                #end_of_batch = (batches_run+1)*batch_size
        j = j + 1
    """
    pass_no = pass_no + 1
    print("Pass: " + str(pass_no))
    print("Refining on whole set")
    # train on final set to better accuracy
    for i in range(no_iterations+1):
        start = time.time()
        sess.run(train_step, feed_dict={
            X: x_train, Y: y_train, keep_prob: dropout
            })
        # print loss and accuracy
        if i % 10 == 0:
            itteration_loss, itteration_accuracy = sess.run(
                [cross_entropy, accuracy],
                feed_dict={X: x_train, Y: y_train, keep_prob: 1.0}
                )
            print(
                "Iteration",
                str(i),
                "\t| Loss =",
                str(itteration_loss),
                "\t| Accuracy =",
                str(itteration_accuracy)
                )
            if itteration_accuracy == 1:
                 break
            end = time.time()
            time_remaining = (end-start) * (no_iterations - i)
            if end-time_remaining < 60:
                print("Estimated Time Remaining : {:.2f} seconds".format(time_remaining))
            elif (time_remaining)/60 < 60:
                print("Estimated Time Remaining : {:.2f} minuites".format(time_remaining / 60))
            else:
                print("Estimated Time Remaining : {:.2f} hours".format((time_remaining / 60)/60))
    
    
    print("Saving Session")
    #saver = tf.train.Saver()
    saver.save(sess, 'trained_network_pictobot')
    #tf.train.export_meta_graph(filename='trained_network_pictobot.meta')
      
    #Test
    print("Testing on Unseen Data")
    itteration_loss, itteration_accuracy = sess.run(
            [cross_entropy, accuracy],
            feed_dict={X: x_train, Y: y_train, keep_prob: 1.0}
            )
    print(
        "Test",
        "\t| Loss =",
        str(itteration_loss),
        "\t| Accuracy =",
        str(itteration_accuracy))