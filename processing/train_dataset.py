import tensorflow.compat.v1 as tf
tf.disable_v2_behavior() 
import pickle
import numpy as np

n_input = 784  # input layer (28x28 pixels)
n_hidden1 = 1024  # 1st hidden layer
n_hidden2 = 512  # 2nd hidden layer
n_hidden3 = 256  # 3rd hidden layer
n_output = 345  # output layer (0-9 digits)

learning_rate = 1e-4
n_iterations = 100
batch_size = 128
dropout = 0.5

file = open("500000\\x_train.obj",'rb')
object_file = pickle.load(file)
x_train = object_file
file.close()

file = open("500000\\x_test.obj",'rb')
object_file = pickle.load(file)
x_test = object_file
file.close()

file = open("500000\\y_train.obj",'rb')
object_file = pickle.load(file)
y_train = object_file
y_train =  np.eye(n_output)[y_train]
file.close()

file = open("500000\\y_test.obj",'rb')
object_file = pickle.load(file)
y_test = object_file
y_test = np.eye(n_output)[y_test]
file.close()

X = tf.placeholder("float", [None, n_input])
Y = tf.placeholder("float", [None, n_output])
keep_prob = tf.placeholder(tf.float32)

weights = {
    'w1': tf.Variable(tf.truncated_normal([n_input, n_hidden1], stddev=0.1)),
    'w2': tf.Variable(tf.truncated_normal([n_hidden1, n_hidden2], stddev=0.1)),
    'w3': tf.Variable(tf.truncated_normal([n_hidden2, n_hidden3], stddev=0.1)),
    'out': tf.Variable(tf.truncated_normal([n_hidden3, n_output], stddev=0.1)),
}

biases = {
    'b1': tf.Variable(tf.constant(0.1, shape=[n_hidden1])),
    'b2': tf.Variable(tf.constant(0.1, shape=[n_hidden2])),
    'b3': tf.Variable(tf.constant(0.1, shape=[n_hidden3])),
    'out': tf.Variable(tf.constant(0.1, shape=[n_output]))
}

layer_1 = tf.add(tf.matmul(X, weights['w1']), biases['b1'])
layer_2 = tf.add(tf.matmul(layer_1, weights['w2']), biases['b2'])
layer_3 = tf.add(tf.matmul(layer_2, weights['w3']), biases['b3'])
layer_drop = tf.nn.dropout(layer_3, keep_prob)
output_layer = tf.matmul(layer_3, weights['out']) + biases['out']

cross_entropy = tf.reduce_mean(
    tf.nn.softmax_cross_entropy_with_logits(
        labels=Y, logits=output_layer # labels=Y
        ))
train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)

correct_pred = tf.equal(tf.argmax(output_layer, 1), tf.argmax(Y, 1))
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

init = tf.global_variables_initializer()
sess = tf.Session()
sess.run(init)

# train
for i in range(n_iterations):
    sess.run(train_step, feed_dict={
        X: x_train, Y: y_train, keep_prob: dropout
        })
    # print loss and accuracy
    if i % 1 == 0:
        itteration_loss, itteration_accuracy = sess.run(
            [cross_entropy, accuracy],
            feed_dict={X: x_train, Y: y_train, keep_prob: 1.0}
            )
        print(
            "Iteration",
            str(i),
            "\t| Loss =",
            str(itteration_loss),
            "\t| Accuracy =",
            str(itteration_accuracy)
            )