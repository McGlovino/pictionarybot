import numpy as np
from sklearn.model_selection import train_test_split
from os import walk, getcwd
import PIL
from PIL import Image
import pickle
import matplotlib.pyplot as plt


##Place all the npy quickdraw files here:
#path = "/home/paoloftuna/quickdraw-dataset-master/Data/"
path = "D:\\Documents\\Uni\\Third year\\quickdraw dataset\\"
txt_name_list = []
for (dirpath, dirnames, filenames) in walk(path):
    txt_name_list.extend(filenames)
    break


x_train = []
x_test = []
y_train = []
y_test = []
xtotal = []
ytotal = []
slice_train = int(500000/len(txt_name_list))  ###Setting value to be 1000 for the final dataset
i = 0
seed = np.random.randint(1, 10e6)


##Creates test/train split with quickdraw data
for txt_name in txt_name_list:
    txt_path = path + txt_name
    print(txt_path)
    x = np.load(txt_path)
    x = x.astype('float32') / 28    ##scale images
    y = [i] * len(x)  
    np.random.seed(seed)
    np.random.shuffle(x)
    np.random.seed(seed)
    np.random.shuffle(y)
    x = x[:slice_train]
    y = y[:slice_train]
    if i != 0: 
        xtotal = np.concatenate((x,xtotal), axis=0)
        ytotal = np.concatenate((y,ytotal), axis=0)
    else:
        xtotal = x
        ytotal = y
    i += 1
        
x_train, x_test, y_train, y_test = train_test_split(xtotal, ytotal, test_size=0.2, random_state=42)

"""import matplotlib.pyplot as plt
output = x_train[1].reshape(28,28)
plt.imshow(output)
plt.show"""

#Write objects
filehandler = open("x_train.obj","wb")
pickle.dump(x_train, filehandler)
filehandler.close()

filehandler = open("x_test.obj","wb")
pickle.dump(x_test, filehandler)
filehandler.close()

filehandler = open("y_train.obj","wb")
pickle.dump(y_train, filehandler)
filehandler.close()

filehandler = open("y_test.obj","wb")
pickle.dump(y_test, filehandler)
filehandler.close()


