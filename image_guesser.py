import os
import tensorflow as tf
import numpy as np
tf.logging.set_verbosity(tf.logging.ERROR)

def guess_image(pil_im, saver):
    np_im = np.array(pil_im)
    np_im = np.reshape(np_im, (1, 784))
    np_im = np_im.astype('float32') / 28
    
    #np_im = tf.convert_to_tensor(np_im, dtype=tf.float32)
    
    #Graph is not all loaded items from default graph
    graph = tf.get_default_graph()
    
    
    with tf.Session() as sess:
        #load items into default graph
        #saver = tf.train.import_meta_graph('network\\trained_network_pictobot.meta')
        saver.restore(sess, tf.train.latest_checkpoint('./network'))
        #saver.restore(sess, tf.train.latest_checkpoint('/home/pi/Documents/PictoBot/network'))

        #prediction = pred.eval({graph.get_tensor_by_name('X:0'): np_im})
        
        prediction = sess.run(tf.argmax(graph.get_tensor_by_name('output_layer:0'), 1), feed_dict={graph.get_tensor_by_name('X_1:0'): np_im})
        
        
        return np.squeeze(prediction)
    