from tkinter import *
from tkinter import colorchooser
from PIL import Image, ImageDraw

class main:
    def __init__(self,master):
        self.master = master
        self.color_fg = 'black'
        self.color_bg = 'white'
        self.old_x = None
        self.old_y = None
        self.pen_width = 5
        self.Draw_Widgets()
        self.canvas.bind('<B1-Motion>',self.Paint)#drwaing the line 
        self.canvas.bind('<ButtonRelease-1>',self.Reset)
        self.Clear()

    def Paint(self,current_pos):
        if self.old_x and self.old_y:
            #tkinter
            self.canvas.create_line(self.old_x,self.old_y,current_pos.x,current_pos.y,width=self.pen_width,fill=self.color_fg,capstyle=ROUND,smooth=True)
            #PIL
            self.draw.line((self.old_x,self.old_y,current_pos.x,current_pos.y), 'black', self.pen_width)
            self.Save_Image()

        self.old_x = current_pos.x
        self.old_y = current_pos.y

    def Reset(self,e):    #reseting mouse 
        self.old_x = None
        self.old_y = None      

    def Change_Width(self,new_width): #change Width of pen
        self.pen_width = new_width
           

    def Clear(self):
        #tkinter
        self.canvas.delete(ALL)
        #PIL
        self.image = Image.new("RGBA", (500, 400), (0, 0, 0, 0))
        self.draw = ImageDraw.Draw(self.image)
        self.Save_Image()

    def Change_fg(self):  #changing the pen color
        self.color_fg=colorchooser.askcolor(color=self.color_fg)[1]

    def Change_bg(self):  #changing the background color canvas
        self.color_bg=colorchooser.askcolor(color=self.color_bg)[1]
        self.canvas['bg'] = self.color_bg

    def Draw_Widgets(self):
        self.controls = Frame(self.master,padx = 5,pady = 5)
        Label(self.controls, text='Pen Width:',font=('arial 18')).grid(row=0,column=0)
        self.slider = Scale(self.controls,from_= 5, to = 100,command=self.Change_Width,orient=VERTICAL)
        self.slider.set(self.pen_width)
        self.slider.grid(row=0,column=1,ipadx=20)
        self.controls.pack(side=LEFT)

        #tkinter canvas for visible drawing
        self.canvas = Canvas(self.master,width=500,height=400,bg=self.color_bg,)
        self.canvas.pack(fill=BOTH,expand=True)
        #PIL image for in memory drawing
        self.image = Image.new("RGB", (500, 400), self.color_bg)
        self.draw = ImageDraw.Draw(self.image)

        menu = Menu(self.master)
        self.master.config(menu=menu)
        filemenu = Menu(menu)
        colormenu = Menu(menu)
        menu.add_cascade(label='Colors',menu=colormenu)
        colormenu.add_command(label='Brush Color',command=self.Change_fg)
        colormenu.add_command(label='Background Color',command=self.Change_bg)
        optionmenu = Menu(menu)
        menu.add_cascade(label='Options',menu=optionmenu)
        optionmenu.add_command(label='Clear Canvas',command=self.Clear)
        optionmenu.add_command(label='Exit',command=self.master.destroy)

    def Save_Image(self):
        #For converting rather than drawing both
        #self.canvas.postscript(file = "post_script.eps")
        #self.image = Image.open("post_script.eps")
        self.image.save("temp.png", "png")
        
if __name__ == '__main__':
    root = Tk()
    main(root)
    root.title('Application')
    root.mainloop()
