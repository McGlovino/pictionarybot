from tkinter import *
from PIL import Image, ImageTk, ImageDraw
from tkinter import ttk
from tkinter import font
import os
import time

#import tensorflow.compat.v1 as tf
#tf.disable_v2_behavior
import tensorflow as tf

import random

import image_guesser
import numpy as np

from WordDictionary import word_dictionary

#### Resize images to specified dimentions, or by a scalar
def resize_image(im, width = 0, height = 0, ratio_x = 0, ratio_y = 0):
    if ratio_x > 0:
        return im.resize((int(im.width * ratio_x), int(im.height * ratio_y)), Image.ANTIALIAS)
    else:
        return im.resize((width, height), Image.ANTIALIAS)
    
#### Shuts the rasberry pi down
def shut_down():
    os.system("sudo shutdown -h now")
    
#### Fullscreens, or unfullscreens the application
def toggle_fullscreen(event):
    if app.attributes('-fullscreen') == 0:
        app.attributes('-fullscreen', 1)
    else:
        app.attributes('-fullscreen', 0)   

### Defining colours
bg_colour = '#eaf0f8';
text_colour = '#494949'
highlight_text_colour = '#b2b2b2'


#### Custom button class, has defaults in, so they dont have to be set each time
class MyButton(Button):
    def __init__(self, master, **kw):
        Button.__init__(self,master=master, **kw)
        self['foreground']=text_colour
        self['activeforeground']=highlight_text_colour
        self['background']=bg_colour
        self['activebackground']=bg_colour
        #button_img = ImageTk.PhotoImage(resize_image(Image.open("buttonPlan_long.png"), ratio = 0.28))
        self['image'] = master.master.button_img
        self['font']=('URW Gothic L', 20, 'normal')
        self['bd']=0
        self['highlightthickness']=0
        self['compound']="center"
        #self.bind("<Enter>", self.on_enter)
        #self.bind("<Leave>", self.on_leave)
        
class App(Tk):
    def __init__(self):
        Tk.__init__(self)
        self._frame = None
        ## Size - Uncomment what not wanted
        width = self.winfo_screenwidth()
        height = self.winfo_screenheight()
        width = 800
        height = 480
        self.geometry(("%dx%d+0+0" % (width, height)))
        self.attributes('-fullscreen', True)
        self.configure(background=bg_colour)
        ## Get images
        ##PC
        self.button_img = ImageTk.PhotoImage(resize_image(Image.open("buttonPlan_long.png"), ratio_x = 0.28, ratio_y = 0.28))
        self.button_img_small = ImageTk.PhotoImage(resize_image(Image.open("buttonPlan_long.png"), ratio_x = 0.18, ratio_y = 0.28))
        self.button_img_smaller = ImageTk.PhotoImage(resize_image(Image.open("buttonPlan_long.png"), ratio_x = 0.07, ratio_y = 0.25))
        self.main_img = ImageTk.PhotoImage(resize_image(Image.open("front_image.png"),width = 325, height = 325))
        ##Command Line
        """self.button_img = ImageTk.PhotoImage(resize_image(Image.open("/home/pi/Documents/PictoBot/buttonPlan_long.png"), ratio_x = 0.28, ratio_y = 0.28))
        self.button_img_small = ImageTk.PhotoImage(resize_image(Image.open("/home/pi/Documents/PictoBot/buttonPlan_long.png"), ratio_x = 0.18, ratio_y = 0.28))
        self.button_img_smaller = ImageTk.PhotoImage(resize_image(Image.open("/home/pi/Documents/PictoBot/buttonPlan_long.png"), ratio_x = 0.07, ratio_y = 0.25))
        self.main_img = ImageTk.PhotoImage(resize_image(Image.open("/home/pi/Documents/PictoBot/front_image.png"),width = 325, height = 325))"""
        ## Drawn image
        self.image = Image.new("RGBA", (380, 380), (0, 0, 0, 0))
        self.image_for_display = Image.new("RGBA", (380, 380), (0, 0, 0, 0))
        self.draw = ImageDraw.Draw(self.image)
        self.draw_for_display = ImageDraw.Draw(self.image_for_display)
        self.imageSmall = Image.new("L", (28, 28))
        ## Word Guess
        self.word = ""
        self.guessing = False
        self.time_last_guess = 0
        self.saver = tf.train.import_meta_graph('network\\trained_network_pictobot.meta')
        #self.saver = tf.train.import_meta_graph('/home/pi/Documents/PictoBot/network/trained_network_pictobot.meta')
        ## Scoring
        self.score_play = 0
        self.total_play = 0
        self.human_vs = 0
        self.pictobot_vs = 0
        self.play_mode = True;
        ## Key binding
        self.bind("<F11>",toggle_fullscreen)
        self.bind("q",toggle_fullscreen)
        #self.bind("g",self.make_guess)
        ## Go to first frame
        self.switch_frame(MainMenu)
        
        #old_x = 0
        #old_y = 0
        
    #### Switching between frames
    def switch_frame(self, frame_class, play_mode = True):
        self.play_mode = play_mode
        if self._frame is not None:
            for widget in self.winfo_children():
                widget.grid_forget()
                widget.destroy()
        new_frame = frame_class(self)
        self._frame = new_frame
        self._frame.grid()
        self.reset_scores()
        
    def make_guess(self, frame):
        #if (time.clock() - self.time_last_guess) > 2:
        
        if self.guessing == False and (time.clock() - self.time_last_guess) > 2:
            self.guessing = True
            self.word = word_dictionary[image_guesser.guess_image(self.imageSmall, self.saver)]
            print("Prediction: ", self.word)
            
            frame.update_screen()
            self.guessing = False
            self.time_last_guess = time.clock()
    
    def reset_scores(self):
        self.score_play = 0
        self.total_play = 0
        self.human_vs = 0
        self.pictobot_vs = 0
        

        
        
class MainMenu(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)       
        ### Spacing out window
        ## Left
        frame_main_left = Frame(master, width=380, height = 480, bg =bg_colour)
        frame_main_left.grid(row=0,column=0)
        frame_main_left.grid_propagate(0)
        ## Right
        frame_main_right = Frame(master, width=420, height = 480, bg =bg_colour)
        frame_main_right.grid(row=0,column=1)
        frame_main_right.grid_propagate(0)
        
        ### Main screen items
        ## Title
        pictobot_text = Label(frame_main_right, text="PictoBot", fg='#494949', bg=bg_colour, font="none 40 bold",anchor="center")
        pictobot_text.grid(row = 0, column = 0, pady=10)
        ## Image
        main_image = Label(frame_main_right, image=master.main_img, bg=bg_colour,anchor="center")
        main_image.grid(row=1, column=0, pady=20)
        ## Top spacer for buttons
        spacer_frame = Frame(frame_main_left, height = 60, bg =bg_colour)
        spacer_frame.grid(row=0, column = 0)
        ## Buttons
        play_mode_btn = MyButton(master=frame_main_left, text="Play Mode", command=lambda: master.switch_frame(VersusPage, True))
        play_mode_btn.grid(row=1, column=0, padx=20, pady=0)
        vs_mode_btn = MyButton(master = frame_main_left, text="Versus Mode", command=lambda: master.switch_frame(VersusPage, False))
        vs_mode_btn.grid(row=2, column=0, padx=20, pady=0)
        word_dict_btn = MyButton(master = frame_main_left, text="Word Dictionary", command=lambda: master.switch_frame(WordDictPage))
        word_dict_btn.grid(row=3, column=0, padx=20, pady=0)
        #colours_btn = MyButton(master = frame_main_left, text="Colours", command=lambda: master.switch_frame(WordDictPage))
        #colours_btn.grid(row=4, column=0, padx=20, pady=0)
        shut_down_btn = MyButton(master = frame_main_left, text="Shut Down", command=shut_down)
        shut_down_btn.grid(row=5, column=0, padx=20, pady=0)

class VersusPage(Frame):
    def __init__(self, master):
        self.suggestions_on = False
        
        Frame.__init__(self, master)
        ### Spacing out window
        ## Left
        self.frame_main_left = Frame(master, width=380, height = 480, bg =bg_colour)
        self.frame_main_left.grid(row=0,column=0)
        self.frame_main_left.grid_propagate(0)
        ## Right
        self.frame_main_right = Frame(master, width=420, height = 480, bg =bg_colour)
        self.frame_main_right.grid(row=0,column=1)
        self.frame_main_right.grid_propagate(0)

        ### Main screen items
        ## Title
        if self.master.play_mode == True:
            pictobot_text = Label(self.frame_main_right, text="Play Mode", fg='#494949', bg=bg_colour, font="none 40 bold",anchor="center")
            pictobot_text.grid(row = 0, column = 0, pady=10)
        else:
            pictobot_text = Label(self.frame_main_right, text="Versus Mode", fg='#494949', bg=bg_colour, font="none 40 bold",anchor="center")
            pictobot_text.grid(row = 0, column = 0, pady=10)
        
        ## Drawing Area
        self.create_canvas()
        
        ## Top spacer for buttons
        spacer_frame = Frame(self.frame_main_left, height = 15, bg =bg_colour)
        spacer_frame.grid(row=0, column = 0)
        ## Buttons
        pen_btn = MyButton(master=self.frame_main_left, text="Pen", command=lambda: self.to_pen())
        pen_btn['image'] = master.button_img_small
        pen_btn.grid(row=1, column=0, padx=0, pady=0)
        rubber_btn = MyButton(master = self.frame_main_left, text="Rubber", command=lambda: self.to_rubber())
        rubber_btn.grid(row=1, column=1, padx=0, pady=0)
        rubber_btn['image'] = master.button_img_small
        clear_btn = MyButton(master=self.frame_main_left, text="Clear", command=lambda: self.clear())
        clear_btn.grid(row=2, column=0, padx=0, pady=0)
        clear_btn['image'] = master.button_img_small
        suggestions_btn = MyButton(master =self. frame_main_left, text="Suggestions", command=lambda: self.suggestions())
        suggestions_btn.grid(row=2, column=1, padx=0, pady=0)
        suggestions_btn['image'] = master.button_img_small
        if self.master.play_mode == True:
            yes_btn = MyButton(master = self.frame_main_left, text="Yes", command=lambda: self.up_play_score(correct = True))
            yes_btn.grid(row=5, column=0, padx=0, pady=0)
            yes_btn['image'] = master.button_img_small
            skip_btn = MyButton(master = self.frame_main_left, text="Skip", command=lambda: self.up_play_score(correct = False))
            skip_btn.grid(row=5, column=1, padx=0, pady=0)
            skip_btn['image'] = master.button_img_small
        else:
            yes_btn = MyButton(master = self.frame_main_left, text="PictoBot", command=lambda: self.up_vs_score(human = False))
            yes_btn.grid(row=5, column=0, padx=0, pady=0)
            yes_btn['image'] = master.button_img_small
            skip_btn = MyButton(master = self.frame_main_left, text="Human", command=lambda: self.up_vs_score(human = True))
            skip_btn.grid(row=5, column=1, padx=0, pady=0)
            skip_btn['image'] = master.button_img_small
        back_btn = MyButton(master = self.frame_main_left, text="\u2B05 Back", command=lambda: self.return_to_menu())
        back_btn.grid(row=6, column=0, padx=0, pady=0, rowspan = 2)
        back_btn['image'] = master.button_img_small
        ## Text
        self.is_it_txt = Label(master = self.frame_main_left, text="Start drawing", fg='#494949', bg=bg_colour, font=('URW Gothic L', 30, 'normal'),anchor="center")
        self.is_it_txt.grid(row =3, column = 0, pady=0, columnspan = 2)
        self.guess_txt = Label(master =self.frame_main_left, text="so I can guess", fg='#494949', bg=bg_colour, font=('URW Gothic L', 30, 'normal'),anchor="center")
        self.guess_txt.grid(row =4, column = 0, pady=0, columnspan = 2)
        self.score_txt = Label(master = self.frame_main_left, text="0/0", fg='#494949', bg=bg_colour, font=('URW Gothic L', 25, 'normal'),anchor="center")
        self.score_txt.grid(row =6, column = 1, pady=0)
        self.guessed_txt = Label(master = self.frame_main_left, text="Guessed", fg='#494949', bg=bg_colour, font=('URW Gothic L', 15, 'normal'),anchor="center")
        self.guessed_txt.grid(row =7, column = 1, pady=0)
    
    ####Reset the text when making a guess
    def update_screen(self):
        self.is_it_txt.grid_forget()
        self.is_it_txt.destroy()
        self.is_it_txt = Label(master = self.frame_main_left, text="Is it {}".format(self.get_prefix(self.master.word.lower())), fg='#494949', bg=bg_colour, font=('URW Gothic L', 30, 'normal'),anchor="center")
        self.is_it_txt.grid(row =3, column = 0, pady=0, columnspan = 2)
        self.guess_txt.grid_forget()
        self.guess_txt.destroy()
        self.guess_txt = Label(master =self.frame_main_left, text="{} ?".format(self.master.word), fg='#494949', bg=bg_colour, font=('URW Gothic L', 30, 'normal'),anchor="center")
        self.guess_txt.grid(row =4, column = 0, pady=0, columnspan = 2)
        self.score_txt.grid_forget()
        self.score_txt.destroy()
        if self.master.play_mode:
            self.score_txt = Label(master = self.frame_main_left, text="{}/{}".format(self.master.score_play, self.master.total_play), fg='#494949', bg=bg_colour, font=('URW Gothic L', 25, 'normal'),anchor="center")
            self.score_txt.grid(row =6, column = 1, pady=0)
        else:
            self.score_txt = Label(master = self.frame_main_left, text="{}/{}".format(self.master.pictobot_vs, self.master.human_vs), fg='#494949', bg=bg_colour, font=('URW Gothic L', 25, 'normal'),anchor="center")
            self.score_txt.grid(row =6, column = 1, pady=0)
        self.guessed_txt.grid_forget()
        self.guessed_txt.destroy()
        self.guessed_txt = Label(master = self.frame_main_left, text="Guessed", fg='#494949', bg=bg_colour, font=('URW Gothic L', 15, 'normal'),anchor="center")
        self.guessed_txt.grid(row =7, column = 1, pady=0)
        
    ####Increases scores
    def up_play_score(self, correct):
        self.clear()
        if correct:
            self.master.score_play += 1    
        self.master.total_play += 1
        self.update_screen()
    
    def up_vs_score(self, human):
        self.clear()
        if human:
            self.master.human_vs += 1
        else:
            self.master.pictobot_vs += 1
        self.update_screen()
        
    
    ####Get the correct prefix  for teh guessed word
    def get_prefix(self, word):
        if word[:3] == 'the':
            return ''
        elif word[1:] == 's':
            return ''
        elif word[:1] == 'a' or word[:1] == 'e' or word[:1] == 'i' or word[:1] == 'o' or word[:1] == 'u':
            return 'an'
        else:
            return 'a'
    
    ####Creating the canvas
    def create_canvas(self):
        self.old_x = None
        self.old_y = None
        self.pen_colour ='#494949'
        self.pen_colour_cur = self.pen_colour
        self.pen_width = 12
        self.canvas = Canvas(self.frame_main_right,width=380,height=380,bg=bg_colour, bd=2)
        self.canvas.grid(row=6, column=0, pady=0)
        self.canvas.bind('<B1-Motion>', self.paint) 
        self.canvas.bind('<ButtonRelease-1>', self.reset)
        self.suggestions_on = False
        
    #### Drawing Lines
    def paint(self, current_pos):
        if self.old_x is not None and self.old_y is not None:
            ##tkinter
            self.canvas.create_line(self.old_x,self.old_y,current_pos.x,current_pos.y,width=self.pen_width,fill=self.pen_colour_cur,capstyle=ROUND,smooth=True)
            ##PIL
            self.master.draw.line((self.old_x,self.old_y,current_pos.x,current_pos.y), 'black', self.pen_width)
            self.master.draw_for_display.line((self.old_x,self.old_y,current_pos.x,current_pos.y), 'black', 3)
            #self.save_image()

        self.old_x = current_pos.x
        self.old_y = current_pos.y

    ### Resets mouse position
    def reset(self, e):
        self.old_x = None
        self.old_y = None
        
        self.save_image()
        self.master.make_guess(self)

    ### Clears the canvas
    def clear(self):
        ##tkinter
        self.canvas.delete(ALL)
        ##PIL
        self.master.image = Image.new("RGBA", (380, 380), (0, 0, 0, 0))
        self.master.image_for_display = Image.new("RGBA", (380, 380), (0, 0, 0, 0))
        self.master.draw = ImageDraw.Draw(self.master.image)
        self.master.draw_for_display = ImageDraw.Draw(self.master.image_for_display)
        self.save_image()
        
    ### Saves the image
    def save_image(self):
        #start_time = time.clock()
        
        #For converting rather than drawing both
        #self.canvas.postscript(file = "post_script.eps")
        #self.image = Image.open("post_script.eps")
        if self.master.image != Image.new("RGBA", (380, 380), (0, 0, 0, 0)):
            self.master.image.save("temp.png", "png")
            #self.master.image.save("/home/pi/Documents/PictoBot/temp.png", "png")
        
            with Image.open("temp.png") as image:
            #with Image.open("/home/pi/Documents/PictoBot/temp.png") as image:
                ##crop to used space
                np_image = np.array(image)
                image_data_bw = np_image.take(3, axis=2)
                non_empty_columns = np.where(image_data_bw.max(axis=0)>0)[0]
                non_empty_rows = np.where(image_data_bw.max(axis=1)>0)[0]
                cropBox = (min(non_empty_rows), max(non_empty_rows), min(non_empty_columns), max(non_empty_columns))
                np_image = np_image[cropBox[0]:cropBox[1]+1, cropBox[2]:cropBox[3]+1 , :]
                
                ##Convert transparent to white
                np_image[np_image[...,-1]==0] = [255,255,255,1]
                
                temp = Image.fromarray(np_image)
                
                self.master.imageSmall = temp.resize((28,28))
                self.master.imageSmall = self.master.imageSmall.convert('L')
                self.master.imageSmall.save("temp2.png", "png")
                #self.master.imageSmall.save("/home/pi/Documents/PictoBot/temp2.png", "png")
                
                #print("{:.2f}".format(time.clock() - start_time))
                
    ### Saves the image intended for the front and then returns
    def return_to_menu(self):
        self.master.image_for_display.save("front_image.png", "png")
        #self.master.image_for_display.save("front_image.png", "png")
        self.master.switch_frame(MainMenu)

    ### To Pen
    def to_pen(self):
        #change rubber button to be not indented.
        #change pen button to be indented
        #change pen to pen colour
        self.pen_colour_cur = self.pen_colour
        self.pen_width = 10

    ### To Rubber
    def to_rubber(self):
        #change rubber button to be indented.
        #change pen button to be not indented
        #change pen to background colour
        self.pen_colour_cur = bg_colour
        self.pen_width = 35
        
    def suggestions(self):
        #Label(self.frame_main_right, text="Versus Mode", fg='#494949', bg=bg_colour, font="none 40 bold",anchor="center")
        #pictobot_text.grid(row = 0, column = 0, pady=10)
        if not self.suggestions_on:
            random_words = []
            for i in range(5):
                random_words.append(word_dictionary[random.randint(0, len(word_dictionary)-1)])
                
            ## Title
            if self.master.play_mode == True:
                pictobot_text = Label(self.frame_main_right, text="Play Mode", fg='#494949', bg=bg_colour, font="none 40 bold",anchor="center")
                pictobot_text.grid(row = 0, column = 0, pady=10)
            else:
                pictobot_text = Label(self.frame_main_right, text="Versus Mode", fg='#494949', bg=bg_colour, font="none 40 bold",anchor="center")
                pictobot_text.grid(row = 0, column = 0, pady=10)
        
            self.suggestions_label = Label(self.frame_main_right, text = random_words[0],bg=bg_colour, font="none 20 bold", fg='#494949', anchor="center")
            self.suggestions_label.grid(row=1, column=0, pady=0)
            self.suggestions_label2 = Label(self.frame_main_right, text = random_words[1],bg=bg_colour, font="none 20 bold", fg='#494949', anchor="center")
            self.suggestions_label2.grid(row=2, column=0, pady=0)
            self.suggestions_label3 = Label(self.frame_main_right, text = random_words[2],bg=bg_colour, font="none 20 bold", fg='#494949', anchor="center")
            self.suggestions_label3.grid(row=3, column=0, pady=0)
            self.suggestions_label4 = Label(self.frame_main_right, text = random_words[3],bg=bg_colour, font="none 20 bold", fg='#494949', anchor="center")
            self.suggestions_label4.grid(row=4, column=0, pady=0)
            self.suggestions_label5 = Label(self.frame_main_right, text = random_words[4],bg=bg_colour, font="none 20 bold", fg='#494949', anchor="center")
            self.suggestions_label5.grid(row=5, column=0, pady=0)
            self.suggestions_on = True
        else:
            self.suggestions_label.grid_forget()
            self.suggestions_label.destroy()
            self.suggestions_label5.grid_forget()
            self.suggestions_label5.destroy()
            self.suggestions_label2.grid_forget()
            self.suggestions_label2.destroy()
            self.suggestions_label3.grid_forget()
            self.suggestions_label3.destroy()
            self.suggestions_label4.grid_forget()
            self.suggestions_label4.destroy()
            self.suggestions_on = False
        

class WordDictPage(Frame):                  
    def __init__(self, master):
        Frame.__init__(self, master)
        ### Spacing out window
        ## Top Left
        self.frame_main_right = Frame(master, width=420, height = 100, bg =bg_colour)
        self.frame_main_right.grid(row=0,column=1, pady = 0)
        self.frame_main_right.grid_propagate(0)
        ## Top Right
        self.frame_main_left = Frame(master, width=380, height = 100, bg=bg_colour)
        self.frame_main_left.grid(row=0,column=0, pady = 0)
        self.frame_main_left.grid_propagate(0)
        ## Bottom Left
        self.frame_bottom = Frame(master, width=800, height = 380, bg =bg_colour)
        self.frame_bottom.grid(row=1,column=0, columnspan = 2)
        self.frame_bottom.grid_propagate(0)

        ### Main screen items
        ## Title
        pictobot_text = Label(self.frame_main_right, text="Word Dictionary", fg='#494949', bg=bg_colour, font="none 30 bold",anchor="center")
        pictobot_text.grid(row = 0, column = 0, pady=10)
        
        ## Text
        self.is_it_txt = []
        self.x = 0
        while self.x < 40:
            for i in range(4):
                for j in range(10):
                    temp_label = Label(master = self.frame_bottom, text=word_dictionary[self.x], fg='#494949', bg=bg_colour, font=('URW Gothic L', 15, 'normal'),anchor="center")
                    temp_label.grid(row =j, column = i+1, pady=0, padx = 5)
                    self.is_it_txt.append(temp_label)
                    self.x = self.x+1
        ## Buttons
        left_btn = MyButton(master = self.frame_bottom, text="\u25C0", command=lambda: self.last_page())
        left_btn.place(x= 0, y = -70, relheight=1)
        left_btn['image'] = master.button_img_smaller
        right_btn = MyButton(master = self.frame_bottom, text="\u25B6", command=lambda: self.next_page())
        right_btn.place(x= 720, y = -70, relheight=1)#.grid(row=3, column=6, padx=0, pady=0, rowspan = 3, sticky="E")
        right_btn['image'] = master.button_img_smaller
        back_btn = MyButton(master = self.frame_bottom, text="\u2B05 Back", command=lambda: master.switch_frame(MainMenu))
        back_btn.grid(row=11, column=0, padx=0, pady=0, columnspan = 2)
        back_btn['image'] = master.button_img_small

        ## Spacer
        spacer_frame = Frame(self.frame_bottom, width = 85, bg =bg_colour)
        spacer_frame.grid(row=0, column = 0)

    def next_page(self):
        if self.x > len(word_dictionary)-1:
            return
        for i in range(len(self.is_it_txt)):
            self.is_it_txt[i].grid_forget()
            self.is_it_txt[i].destroy()
        self.is_it_txt = []
        next_x = self.x + 40
        while self.x < next_x and self.x <len(word_dictionary) :
            for i in range(4):
                if self.x > len(word_dictionary)-1:
                        break
                for j in range(10):
                    temp_label = Label(master = self.frame_bottom, text=word_dictionary[self.x], fg='#494949', bg=bg_colour, font=('URW Gothic L', 15, 'normal'),anchor="center")
                    temp_label.grid(row =j, column = i+1, pady=0, padx = 5)
                    self.is_it_txt.append(temp_label)
                    self.x = self.x+1
                    if self.x > len(word_dictionary)-1:
                        break

    def last_page(self):
        if self.x < 41:
            return
        for i in range(len(self.is_it_txt)):
            self.is_it_txt[i].grid_forget()
            self.is_it_txt[i].destroy()
        self.is_it_txt = []
        if self.x > len(word_dictionary)-1:
            next_x = self.x - (self.x % 40)
            self.x = self.x - (40 + (self.x % 40))
        else:
            next_x = self.x - 40
            self.x = self.x - 80
        while self.x < next_x and self.x <len(word_dictionary) :
            for i in range(4):
                for j in range(10):
                    temp_label = Label(master = self.frame_bottom, text=word_dictionary[self.x], fg='#494949', bg=bg_colour, font=('URW Gothic L', 15, 'normal'),anchor="center")
                    temp_label.grid(row =j, column = i+1, pady=0, padx = 10)
                    self.is_it_txt.append(temp_label)
                    self.x = self.x+1

if __name__ == "__main__":
    app = App()
    #app.mainloop()
    start_time = time.clock()
    x = 5
    counter = 0
    while True:
        app.update_idletasks()
        app.update()
        
        counter +=1
        
        #print(time.clock() - start_time)
        if(time.clock() - start_time) > x:
            #print(counter)
            print("{:.2f}".format((counter / (time.clock() - start_time))/100))
            counter = 0
            start_time = time.clock()
        #print("FPS: ", 1.0 / (time.time() - start_time))
