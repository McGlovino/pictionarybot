from tkinter import *
from PIL import Image, ImageTk
from tkinter import ttk
from tkinter import font
import os

#### Resize images to specified dimentions, or by a scalar
def resize_image(im, width = 350, height = 350, ratio = 0):
    if ratio > 0:
        return im.resize((int(im.width * ratio), int(im.height * ratio)), Image.ANTIALIAS)
    else:
        return im.resize((width, height), Image.ANTIALIAS)

#### Changing between different windows of the project
def change_window():
    Label(window, text="Cool",bg  ="white", fg="red", font="none 30 bold").grid(row = 3, column = 1)
#### Shuts the rasberry pi down
def shut_down():
    os.system("sudo shutdown -h now")
#### Fullscreens, or unfullscreens the application
def toggle_fullscreen(event):
    if window.attributes('-fullscreen') == 0:
        window.attributes('-fullscreen', 1)
    else:
        window.attributes('-fullscreen', 0)

#### Custom button class, has defaults in, so they dont have to be set each time
class MyButton(Button):
    def __init__(self, master, **kw):
        Button.__init__(self,master=master, **kw)
        self['foreground']=text_colour
        self['activeforeground']=highlight_text_colour
        self['background']=bg_colour
        self['activebackground']=bg_colour
        self['image'] = button_img
        self['font']=('URW Gothic L', 20, 'normal')
        self['bd']=0
        self['highlightthickness']=0
        self['compound']="center"
        #self.bind("<Enter>", self.on_enter)
        #self.bind("<Leave>", self.on_leave)


### Defining colours
bg_colour = '#eaf0f8';
text_colour = '#494949'
highlight_text_colour = '#b2b2b2'


### Create window
window = Tk()
window.title("PictoBot")
## Size - Uncomment what not wanted
width = window.winfo_screenwidth()
height = window.winfo_screenheight()
width = 800
height = 480
window.geometry(("%dx%d+0+0" % (width, height)))
window.attributes('-fullscreen', True)
window.configure(background=bg_colour)
## Key binding
window.bind("<F11>",toggle_fullscreen)
window.bind("q",toggle_fullscreen)

### Load images
button_img = ImageTk.PhotoImage(resize_image(Image.open("buttonPlan_long.png"), ratio = 0.28))


### Spacing out window
## Left
frame_main_left = Frame(window, width=400, height = 480, bg =bg_colour)
frame_main_left.grid(row=0,column=0)
frame_main_left.grid_propagate(0)
## Right
frame_main_right = Frame(window, width=400, height = 480, bg =bg_colour)
frame_main_right.grid(row=0,column=1)
frame_main_right.grid_propagate(0)
frame_main_right_top = Frame(frame_main_right, width=400, height = 120, bg =bg_colour)
frame_main_right_top.grid(row=0,column=0)
frame_main_right_top.grid_propagate(0)
frame_main_right_bottom = Frame(frame_main_right, width=400, height = 360, bg =bg_colour)
frame_main_right_bottom.grid(row=1,column=0)
frame_main_right_bottom.grid_propagate(0)

### Main screen items
## Title
pictobot_text = Label(frame_main_right_top, text="PictoBot", fg='#494949', bg=bg_colour, font="none 40 bold")
pictobot_text.grid(row = 0, column = 0, padx=40, pady=20)
## Image
photo1 = ImageTk.PhotoImage(resize_image(Image.open("temp.png")))
main_image = Label(frame_main_right_bottom, image=photo1, bg=bg_colour,anchor="center")
main_image.grid(row=0, column=0, padx=10, pady=0)
## Top spacer for buttons
spacer_frame = Frame(frame_main_left, height = 15, bg =bg_colour)
spacer_frame.grid(row=0, column = 0)
## Buttons
play_mode_btn = MyButton(master = frame_main_left, text="Play Mode", command=change_window)
play_mode_btn.grid(row=1, column=0, padx=20, pady=0)
vs_mode_btn = MyButton(master = frame_main_left, text="Versus Mode", command=change_window)
vs_mode_btn.grid(row=2, column=0, padx=20, pady=0)
word_dict_btn = MyButton(master = frame_main_left, text="Word Dictionary", command=change_window)
word_dict_btn.grid(row=3, column=0, padx=20, pady=0)
colours_btn = MyButton(master = frame_main_left, text="Colours", command=change_window)
colours_btn.grid(row=4, column=0, padx=20, pady=0)
colours_btn = MyButton(master = frame_main_left, text="Shut Down", command=shut_down)
colours_btn.grid(row=5, column=0, padx=20, pady=0)


window.mainloop()
